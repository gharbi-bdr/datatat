terraform {
  backend "http" {
  }
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.0.0"
    }
  }
}

provider "google" {
  project = "simple-331916"
  credentials = file("service_account.json")
}

resource "google_cloud_run_service" "default" {
  name     = "cloudrun-srvnnn"
  location = "us-central1"

  template {
    spec {
      containers {
        image = "us-docker.pkg.dev/cloudrun/container/hello"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }
}

